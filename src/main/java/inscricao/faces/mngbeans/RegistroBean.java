package inscricao.faces.mngbeans;


import inscricao.entity.Candidato;
import utfpr.faces.support.PageBean;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by André on 18/10/2016.
 */
@ApplicationScoped
@Named
public class RegistroBean extends PageBean{
    private ArrayList<Candidato> CandidatosList = new ArrayList<>();


    public void save(Candidato candidato)
    {
        this.CandidatosList.add(candidato);
    }

    public List<Candidato> getCandidatosList()
    {
        return CandidatosList;
    }




}
